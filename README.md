# Shell
## Projet programmation Windows/Linux
---

### Installation

``` sh
make shell
``` 

## Lancer le shell

```sh
./shell
```

## Historique des commandes

```sh
history
```


### Auteurs

Martin Costes

Axel Ferreol