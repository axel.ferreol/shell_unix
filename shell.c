#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define COMMAND_LENGTH_MAX 100
#define PATH_MAX 100
#define BUFFER_LENGTH 2000



//returns the content of history file
void history(char* path){
    char path_file[PATH_MAX];
    strcpy(path_file,path);
    strcat(path_file,"/history");
    FILE* fptr = fopen(path_file,"r");
    if(fptr == NULL){
        fprintf(stdout,"ERROR ; in opening history\n");
    }
    else {
        char line[100];
        while (fgets(line, 100,fptr))
        {
            fprintf(stdout,"%s",line);
        }

        fclose(fptr);

    }
}

// concatenate transforms argv into string
// /!\ needs to be FREED
char* concatenate(char** argv){
    char* str = malloc(COMMAND_LENGTH_MAX * sizeof(char));
    str[0] = '\0';
    int i = 0;
    while(argv[i] != NULL){
        strcat(str,argv[i]);
        strcat(str," ");
        ++i;
    }
    return str;

}


//add_history add the command to history file
void add_history(char* cmd, char* path){
    char path_file[PATH_MAX];
    strcpy(path_file,path);
    strcat(path_file,"/history");
    FILE* fptr = fopen(path_file,"a");
    if(fptr == NULL){
        fprintf(stderr,"ERROR : in opening file\n");
        exit(1);
    };

    if (fprintf(fptr,"%s\n", cmd) < 0){
        fprintf(stderr,"ERROR in writting in fptr\n");
    }
    if (fclose(fptr) != 0){
        fprintf(stderr, "ERROR in closing fptr\n");
    }
}


//[parser(cmd)] parses cmd and return a pointer to an argv
// /!\ needs to be FREED !
char** parser(char* cmd){
    if (cmd == NULL){
        return NULL;
    }
    int space_count = 0;
    
    {int i = 0;
    while(cmd[i] != '\0'){
        if (cmd[i] == ' ')
        {
            ++space_count;
        }
        ++i;
    }}

    char** argv = malloc((space_count+2)* sizeof(char*));
    argv[0] = strtok(cmd," "); 

    //Never executed if no space
    for (int i = 0; i<space_count; ++i){
        argv[i+1] = strtok(NULL," ");
    }
    //Need to close argv by NULL
    argv[space_count+1] = NULL;

    //Debug in parser
    /*
    printf("\nPARSER :\n");
    {int i = 0;
    while(argv[i] != NULL){
        printf("arg[%d] : %s\n",i,argv[i]);
        ++i;
    }}
    */
    return argv;
}

// [local_function(argv)] tests if argv is an internal command of the shell
// If so, it executes it
// Internal commands are : "history", ""
int local_funtions(char** argv,char* path){

    if(argv == NULL){
        exit(1);
    }

    if (strcmp(argv[0],"history") == 0){
        history(path);
        return 1;
    }
    return -1;

}


struct l_proc {
    char* str;
    char** argv;
    struct l_proc* next;
};

//[getlast(list_proc)] returns the last l_proc of the list l_proc
struct l_proc* getlast(struct l_proc* list_proc){
    while(list_proc -> next !=NULL){
        list_proc = list_proc -> next;
    }
    return list_proc;
}


// [remove_proc(l_proc,rm_proc)] returns a pointer to l_proc where rm_proc has been deleted
// There is no test whether rm_proc belongs to l_proc !
// rm_proc is not freed !
struct l_proc* remove_proc(struct l_proc* list_proc, struct l_proc* rm_proc){
    struct l_proc* begin = list_proc;
    if (list_proc == rm_proc){
        return NULL;
    }
    while(list_proc -> next != rm_proc){
        list_proc = list_proc -> next;
    }
    list_proc->next = NULL;
    return begin;

}


// [display_listproc(list_proc)] displays the list of proc
void display_listproc(struct l_proc* list_proc){

    printf("***Display command***\n");
    struct l_proc* list_print = list_proc;
    while(list_print != NULL){

        printf("Process : %s\n",list_print -> str);
        {
            int i = 0;
            char** argv = list_print -> argv;
            if (argv != NULL){
                while(argv[i] != NULL){
                printf("arg[%d] : %s\n",i,argv[i]);
                    ++i;
            }
            
            printf("arg[%d] : NULL\n",i);
            }
            list_print = list_print -> next;
            printf("\n");
        }
        printf("\n");
    }
}


// [dash(cmd)] parses the command with |, it returns a l_proc 
struct l_proc* dash(char* cmd){
    struct l_proc* upstream = malloc(sizeof(struct l_proc));
    struct l_proc* begin = upstream;

    char* cmd_temp = strtok(cmd, "|");
    upstream -> str = cmd_temp;
    upstream -> next = NULL;

    while(1){
        cmd_temp = strtok(NULL, "|");
        
        if( cmd_temp == NULL){
            break;
        }
        else {
            //ajout
            struct l_proc* downstream = malloc(sizeof(struct l_proc));
            downstream -> str = cmd_temp;
            downstream -> next = NULL;

            upstream -> next = downstream;
            upstream = downstream;
        }
    }
    upstream = begin;

    while(upstream != NULL){
        upstream -> argv = parser(upstream->str);
        upstream = upstream -> next;
        }

    // Debug parser dash
    /*
    printf("PARSER DASH:\n");
    printf("CMD : %s\n",cmd);
    int i = 0;
    struct l_proc* l_print = begin;
    do {
        printf("%d : %s\n",i,l_print->str);
        ++i;
        l_print = l_print -> next;
    } while(l_print != NULL);
    */

    return begin;

}


// [recursive_execution(fds,list_proc)] picks up the last process of list_proc, 
// connects it to the parent process and to its future child process (actually, grand child) and
// executes it
//Always terminate the process
void recursive_execution(int* fds, struct l_proc* list_proc,char* path){
    // If no next process, just terminate the current fork()
    if (list_proc == NULL){
        // null process to terminate
        exit(1);
    }
    else {
        // Get the child process
        struct l_proc* proc = getlast(list_proc);
        list_proc = remove_proc(list_proc,proc);
        
        int fds_child[2];
        pipe(fds_child);
        pid_t p = fork();
        
        if (p == -1){
            printf("Fork has failed\n");
        }

        // Process grand child
        else if (p == 0){
            free(proc -> argv);
            free(proc);

            // Recursively handle the next processes
            recursive_execution(fds_child,list_proc,path);

        }

        // Process Child
        else {
            // Connect to the parent process
            close(fds[0]);
            dup2(fds[1],1);
            // Connect to the grand child process
            close(fds_child[1]);
            dup2(fds_child[0],0);
            
            wait(NULL);
            

            if (local_funtions(proc->argv,path) > 0){
                exit(1);
            }

            if(execvp(proc->argv[0],proc->argv) < 0){
                char* str = concatenate(proc -> argv);
                printf("Failed to exec : %s\n", str);
                free(str);
                exit(1);
            }

        }
    }

}


// [free_l_proc(list_proc)] frees the list of processes : for zach process its argv and its proc
void free_l_proc(struct l_proc* list_proc){
    if (list_proc == NULL){
        return;
    }
    else {
        free_l_proc(list_proc -> next);
        free(list_proc -> argv);
        free(list_proc);
    }
}

// [pre_command(list_proc)] handles internal commands before fork()
// namely 'cd', 'exit'
int pre_command(struct l_proc* list_proc){
    if (list_proc -> argv == NULL){
        return -1;
    }

    if (strcmp(list_proc -> argv[0],"cd") == 0){
        if(chdir(list_proc -> argv[1]) < 0){
            printf("Error chdir : unable to change de path\n");
        }
        return 0;
    }

    if (strcmp(list_proc -> argv[0],"exit") == 0){
        exit(1);
        return 0;
    }

}

void main (){
    char path[PATH_MAX];
    getcwd(path, PATH_MAX);
    char cmd [(COMMAND_LENGTH_MAX * sizeof(char))];

    printf("\nWELCOME TO THE SHELL !\n");
    while(1){    

        //Print PWD
        char cwd[200] ;
        fprintf(stdout,"%s$ ",getcwd(cwd, 100));

        //Get cmd
        fgets(cmd, COMMAND_LENGTH_MAX, stdin); // '\0' stored at the end
        cmd[strlen(cmd)-1] = '\0'; // delete '\n' at the end of cmd;

        // Add to histoty the command
        add_history(cmd,path);

        // Parse the command
        struct l_proc* list_proc = dash(cmd);
        struct l_proc* proc = getlast(list_proc);
        
        // Handle pre_command
        if (pre_command(list_proc) == 0){
            free_l_proc(list_proc);
            continue;
        }
        
        // Display command
        //dsipdisplay_listproc(list_proc);
        
        int fds[2];
        pipe(fds);
        pid_t p = fork();

        if (p == 0){

            recursive_execution(fds,list_proc,path);            
            
        }
        else if (p == -1) {
            fprintf(stderr,"Fork has failed\n");
        } 
        
        else{
            close(fds[1]);
            wait(NULL);
            char buffer_out[BUFFER_LENGTH] = {'\0'};
            read(fds[0],buffer_out,BUFFER_LENGTH);
            printf("%s\n",buffer_out);
            
            //free all processes of list_proc
            free_l_proc(list_proc);
        }
        
      
    }
  
}
